import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BoatUnderwritingInfoComponent } from './boat-underwriting-info.component';

describe('BoatUnderwritingInfoComponent', () => {
  let component: BoatUnderwritingInfoComponent;
  let fixture: ComponentFixture<BoatUnderwritingInfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BoatUnderwritingInfoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BoatUnderwritingInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
