import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BoatCoveragesComponent } from './boat-coverages.component';

describe('BoatCoveragesComponent', () => {
  let component: BoatCoveragesComponent;
  let fixture: ComponentFixture<BoatCoveragesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BoatCoveragesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BoatCoveragesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
