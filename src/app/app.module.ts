import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';


import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { BoatInfoComponent } from './boat-info/boat-info.component';
import { BoatOperatorsComponent } from './boat-operators/boat-operators.component';
import { BoatCoveragesComponent } from './boat-coverages/boat-coverages.component';
import { BoatLocationComponent } from './boat-location/boat-location.component';
import { BoatUnderwritingInfoComponent } from './boat-underwriting-info/boat-underwriting-info.component';
import { BoatOptionalCoveragesComponent } from './boat-optional-coverages/boat-optional-coverages.component';
// import * as $ from 'jquery';
// import * as p from 'Popper';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    BoatInfoComponent,
    BoatOperatorsComponent,
    BoatCoveragesComponent,
    BoatLocationComponent,
    BoatUnderwritingInfoComponent,
    BoatOptionalCoveragesComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
