import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BoatOperatorsComponent } from './boat-operators.component';

describe('BoatOperatorsComponent', () => {
  let component: BoatOperatorsComponent;
  let fixture: ComponentFixture<BoatOperatorsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BoatOperatorsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BoatOperatorsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
