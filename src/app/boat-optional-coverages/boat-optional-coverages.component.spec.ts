import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BoatOptionalCoveragesComponent } from './boat-optional-coverages.component';

describe('BoatOptionalCoveragesComponent', () => {
  let component: BoatOptionalCoveragesComponent;
  let fixture: ComponentFixture<BoatOptionalCoveragesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BoatOptionalCoveragesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BoatOptionalCoveragesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
